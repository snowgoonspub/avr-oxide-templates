#!/bin/bash

# Default chip option fuses for the '4809
#FUSE_OSCCFG=0x01    # 16 MHz
FUSE_OSCCFG=0x02    # 20 MHz
FUSE_SYSCFG0=0xC9   # No CRC, Reset is Reset, don't erase EEPROM
FUSE_BOOTEND=0x00   # Whole Flash is boot

# Determine the programmer type based on DEVICE env variable
case ${DEVICE:-nanoevery} in
  nanoevery)
    TOOL=avrdude
    PROGRAMMER=jtag2updi
    BAUDRATE=115200
    PART=atmega4809
    FUSEFLAGS="-Ufuse2:w:${FUSE_OSCCFG}:m -Ufuse5:w:${FUSE_SYSCFG0}:m -Ufuse8:w:${FUSE_BOOTEND}:m"
    ;;

  uno)
    TOOL=avrdude
    PROGRAMMER=arduino
    BAUDRATE=115200
    PART=atmega328p
    FUSEFLAGS=""
    ;;

  atmelice4809)
    TOOL=avrdude
    PROGRAMMER=atmelice_updi
    PROGRAMMER_OPTS=""
    PART=atmega4809
    FUSEFLAGS="-Ufuse2:w:${FUSE_OSCCFG}:m -Ufuse5:w:${FUSE_SYSCFG0}:m -Ufuse8:w:${FUSE_BOOTEND}:m"
    ;;

  atmelice328p)
    TOOL=avrdude
    PROGRAMMER=atmelice_isp
    PROGRAMMER_OPTS=""
    PART=atmega328p
    ;;

  xplainedpro4809)
    # On the Xplained Pro, we disable Rest so we can use the 2nd button
    FUSE_SYSCFG0=0xC1
    TOOL=avrdude
    PROGRAMMER=xplainedpro_updi
    PROGRAMMER_OPTS="-Ufuse5:w:${FUSE_SYSCFG0}:m"
    PART=atmega4809
    ;;

  xplainedmini328p)
    TOOL=avrdude
    PROGRAMMER=xplainedmini
    PROGRAMMER_OPTS=""
    PART=atmega328p
    ;;

  simavr328p)
    TOOL=simavr
    PART=atmega328p
    FREQ=16000000
    PROGRAMMER_OPTS=""
    ;;

  *)
    echo "Usage"
    echo "  ${0} (nanoevery|uno|atmelice4809|atmelice328p|xplainedpro4809|xplainedmini328p|simavr328p) [--tty <baud> <params>] <file.elf>"
    exit 2
esac


# Gimme the name of a file to load
if [ "X${1}" = "X--tty" ]; then
  TTYBAUD=${2}
  TTYPARAMS=${3}
  shift
  shift
  shift
fi



ELFFILE=${1}

# We need to use the `avrdude` that comes with the Arduino IDE, it seems
# to have some custom changes not in the version we installed from Brew, that
# work with the UPDI-over-USB bootloader on the Arduino Nano Every
AVRDUDE=~/Library/Arduino15/packages/arduino/tools/avrdude/6.3.0-arduino17/bin/avrdude
AVRCONF=~/Library/Arduino15/packages/arduino/tools/avrdude/6.3.0-arduino17/etc/avrdude.conf
SIMAVR=simavr
OBJCOPY=avr-objcopy
SCREEN=screen

"${OBJCOPY}" -O ihex "${ELFFILE}" "${ELFFILE}.hex"

# Where to find it, if we're using a serial attached device (or for `screen` if
# the --tty flag was set)
PORT=$(find /dev/cu.usbmodem* | head -n 1)

if [ "${PROGRAMMER}" = "jtag2updi" ]; then

  PROGRAMMER_OPTS="-P${PORT} -b${BAUDRATE}"

  # We reset the Arduino (and put it into UPDI mode) by opening & closing the
  # serial port at 1200baud (this is some kind of 'backdoor' reset process
  # built into the USB software that runs on the Nano Every's coprocessor
  # for handling USB-to-UPDI.
  stty -f "${PORT}" 1200

  # Wait for the port to be available again
  while [ 1 ]; do
   sleep 0.5
    [ -c "${PORT}" ] && break
  done
fi

if [ "${PROGRAMMER}" = "arduino" ]; then
  PORT=$(find /dev/cu.usbmodem* | head -n 1)
  PROGRAMMER_OPTS="-P${PORT} -b${BAUDRATE}"
fi


# NOW, finally, we can actually upload our code
case ${TOOL:-avrdude} in
  avrdude)
    ${AVRDUDE} \
      -C ${AVRCONF} \
      -v -p${PART} \
      -c${PROGRAMMER} ${PROGRAMMER_OPTS} \
      -e -D \
      -Uflash:w:${ELFFILE}:e \
      ${FUSEFLAGS}

    if [ -n "$TTYBAUD" ]; then
      screen ${PORT} ${TTYBAUD} ${TTYPARAMS}
    fi
    ;;

  simavr)
    ${SIMAVR} -vvv ${PROGRAMMER_OPTS} -f ${FREQ} -m ${PART} -g 1442 ${ELFFILE}
    ;;
esac
