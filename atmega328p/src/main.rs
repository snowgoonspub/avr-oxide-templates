#![no_std]
#![no_main]

use avr_oxide::alloc::boxed::Box;
use avr_oxide::boards::board;
use avr_oxide::StaticWrap;
use avr_oxide::devices::debouncer::Debouncer;
use avr_oxide::devices::{UsesPin, OxideLed, OxideButton, OxideSerialPort};
use avr_oxide::hal::generic::serial::{BaudRate, DataBits, Parity, SerialPortMode, StopBits};

// Code ======================================================================
/**
 * Main function demonstrating the simple on_event based approach to handling
 * device IO.
 */
#[avr_oxide::main(chip="atmega328p")]
pub fn main() -> ! {
  let supervisor = avr_oxide::oxide::instance();

  // Buttons and LEDs setup
  let button = StaticWrap::new(OxideButton::using(Debouncer::with_pin(board::pin_a(2))));
  let green = OxideLed::with_pin(board::pin_d(7));

  // Set an event handler to be called every time someone presses the button
  button.borrow().on_click(Box::new(move |_pinid, _state|{
    green.toggle();
  }));

  // Tell the supervisor what devices to pay attention to, and then enter
  // the main loop.
  supervisor.listen(button.borrow());
  supervisor.run();
}
