# AVRoxide Templates
These are templates for building applications using
the [AVRoxide] runtime for Rust-on-AVR

This version is for version 0.4 of the [AVRoxide crate](https://crates.io/crates/avr-oxide).

## Index
| Directory  | Description                                     |
|------------|-------------------------------------------------|
| atmega4809 | Skeleton application for ATmega4809 controllers |
| atmega328p | Skeleton application for ATmega328P controllers |

## Further information
* [Project homepage](https://avroxi.de)
* [AVRoxide Crate](https://crates.io/crates/avr-oxide)
* [Author's blog](https://snowgoons.ro)

[AVRoxide]: https://avroxi.de/
